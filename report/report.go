package report

import (
	"fmt"
	"os"
	"text/tabwriter"

	"pb1899.dev/galaxy-credentials/credentials"
)

type Printer struct {
	w *tabwriter.Writer
}

func New(file *os.File) *Printer {
	w := tabwriter.NewWriter(file, 3, 0, 3, ' ', tabwriter.TabIndent)
	return &Printer{
		w: w,
	}
}

func (p *Printer) CredentialsHeader() {
	fmt.Fprintln(p.w, "Name\tID\tOrganisation\tURL\tCreatedBy\tCreated")
}

func (p *Printer) CredentialDetails(c credentials.Credential) {
	fmt.Fprintf(p.w, "%v\t%v\t%v\t%v\t%v\t%v\n", c.Name, c.ID, c.SummaryFields.Owners, c.Inputs.URL, c.SummaryFields.CreatedBy.Username, c.Created)
}

func (p *Printer) Cleanup() {
	p.w.Flush()
}
