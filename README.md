### Script to resolve the issue with the missing/duplicate Ansible Galaxy credentials

#### Usage

create a report of the existing credentials:
```bash
go run main.go --report
```

rename the existing duplicate credentials:
```bash
go run main.go --rename
```

