package credentials

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

//$ curl -H "Accept: application/json" -H "Authorization: Bearer ${AAP_TOKEN}" 'https://aap.corp.redhat.com/api/v2/credentials/?credential_type__search=galaxy&page_size=50'

// credentials:
// https://aap.corp.redhat.com/api/v2/credentials/?credential_type__search=galaxy&page_size=50

// credentials for one org:
// https://aap.corp.redhat.com/api/v2/organizations/12/galaxy_credentials/

// one credential:
//https://aap.corp.redhat.com/api/v2/credentials/36/

type Credential struct {
	Name    string `json:"name"`
	ID      int    `json:"id"`
	Org     int    `json:"organization"`
	Created string `json:"created"`
	Inputs  struct {
		URL string `json:"url,omitempty"`
	} `json:"inputs,omitempty"`
	SummaryFields struct {
		CreatedBy struct {
			//ID        int    `json:"id,omitempty"`
			Username string `json:"username"`
		} `json:"created_by"`
		Owners []struct {
			Owner string `json:"name"`
		} `json:"owners,omitempty"`
	} `json:"summary_fields,omitempty"`
}

type Response struct {
	Data []Credential `json:"results"`
}

func GetCredentials(url string) ([]Credential, error) {

	credentials := []Credential{}
	apiToken := os.Getenv("AAP_TOKEN")

	method := "GET"
	client := &http.Client{}
	bearer := "Bearer " + apiToken
	req, err := http.NewRequest(method, url, nil)
	req.Header.Add("Authorization", bearer)

	if err != nil {
		log.Fatal(err)
	}
	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	var r Response

	valid := json.Valid(body)
	if valid {
		json.Unmarshal(body, &r)
	}
	credentials = append(credentials, r.Data...)

	return credentials, nil
}

func (c Credential) Rename(id int) {
	url := fmt.Sprintf("https://aap.corp.redhat.com/api/v2/credentials/%v/", id)
	apiToken := os.Getenv("AAP_TOKEN")
	newName := fmt.Sprintf("AAH-%v", c.SummaryFields.Owners[0].Owner)
	payload := map[string]string{"name": newName, "description": c.Name}

	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		log.Fatal(err)
	}

	method := "PATCH"
	client := &http.Client{}
	bearer := "Bearer " + apiToken
	req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonPayload))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	log.Println(newName, res.Status)
}
