package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"pb1899.dev/galaxy-credentials/credentials"
	"pb1899.dev/galaxy-credentials/report"
)

const (
	credentialsList = "https://aap.corp.redhat.com/api/v2/credentials/?credential_type__search=galaxy&page_size=50"
	YYYYMMDD        = "2006-01-02"
)

func main() {

	createreport := flag.Bool("report", false, "create report in creds_YYYY-MM-DD.txt")
	rename := flag.Bool("rename", false, "rename the credentials to have unique, org-specific names")
	flag.Parse()

	credentials, _ := credentials.GetCredentials(credentialsList)

	if *createreport {

		now := time.Now().UTC()
		filename := fmt.Sprintf("creds_%v.txt", now.Format(YYYYMMDD))

		file, err := os.Create(filename)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		p := report.New(file)
		p.CredentialsHeader()

		for _, c := range credentials {
			p.CredentialDetails(c)
		}

		p.Cleanup()
	}
	if *rename {
		for _, c := range credentials {
			if c.Name == "Ansible Automation Hub (https://cloud.redhat.com/api/automation-hub/)" {
				c.Rename(c.ID)
			}
		}
	}
}
